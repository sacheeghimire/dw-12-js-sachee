/* 
() parenthesis
{} curly braces/block
[]array
 */
//if block gets executed if condition is true
if(true){
    console.log("hello i am if")
}

let name="Sachee"
if(name==="Sachee"){
    console.log("my name is sachee ghimire")
}

//concept of boolean truly and falsy
let a="0"
if(a){
    console.log("hello")
}
let b=""
if(b){
    console.log("hi")
}

