//If any task must be frequently needed then put it into func.

/*
define function
call function 
 */
//defination 
let func1= function(){
    let num1=1
    let num2=2
    console.log(num1+num2)
};
//cannot be executed until called, then ones called goes back to the call part

console.log("hello world")

func1()//jumps to the defined part(calls)

console.log("out of function")


/* 
console;
hello world
3
out of function
 */