// a)make a arrow function name is18 pass a value ,the function must return true if the given age is 18 otherwise return false

let is18=function(age){
    if(age===18){
        return true
    }else{
        return false
    }

}
let s=is18(18)
console.log(s)

console.log()

//b) make a arrow function named isGreaterThan18 , pass a value , the function must return true if the given age is greater or equals to 18 otherwise false

let isGreaterThan18=function(age){
    if(age>=18){
        return true
    }else{
        return false
    }

}
let _isGreaterThan18=isGreaterThan18(18)
console.log(_isGreaterThan18)

console.log()

//c) make a arrow function that take a number and return you can enter room only if the enter number is less than 18 else you can not enter.

let result = function(number) {
    if (number >=1 && number < 18) {
        return ("You can enter the room")
    }
    else {
        return ("You cannot enter the room")
    }
}
let __result=result(20)
console.log(__result)

console.log()



//d) make a arrow function named isEven , pass a value, that return true if the given number is even else return false

let isEven=function(num1){
    if(num1%2===0){
        return true
    }else{
        return false
    }

}
let ___isEven=isEven(11)
console.log(___isEven)

console.log()

//e) make a arrow function that takes 3 input as number and return average of given number

let avg=function(a,b,c){
    let num=(a+b+c)/3
    return num

}
let _avg=avg(1,2,3)
console.log(_avg)

console.log()

// f) make a arrow function that takes one input as number and return "category1" for number range from 1 to10,  return "category2" for number range from 11 to 20, return "category3" for number range form 21 to 30

let whichCategory = function (n1) {
    if ( n1<=1 && n1 <=10) {
        return ("Category 1")
    }
    else if (n1 > 10 && n1 < 21) {
        return("Category 2")
    }
    else if (n1>=21 && n1<=30) {
        return("Category 3")
    }
    else {
        return ("NULL")
    }
}
let category = whichCategory(20)
console.log(category)

console.log()

// g) make a arrow function that takes a input as number  that perform
// 		    if age [upto 17],  return your ticket is free
// 			if age[18 to 25 ], return  your ticket cost 100
// 			if age[>26],  return your ticket cost 200
// 			if age===26 return your ticket is 150

let ticket=function(age){
    if(age<=17){
        return("your ticket is free")
    }
    else if (age<=18 && age<=25){
        return("your ticket costs 100")
    } else if (age===26){
        return("your ticket costs 150")
    }else {
        return("your ticket costs 200")
   }
}
let _ticket=ticket(26)
console.log(_ticket)

console.log()

// h)  make a function that take a number
//  			if number>=3 console i am greater or equal to 3
//  			if number ===3 console i am 3
//  			if number<3 console i am less than3
//  			else show i am other

let _number=function(number){
    if(number<3){
        return("i am less than 3")
    }else if(number===3){
        return("i am 3")
    }else if(number>=3){
        return("i am greater or equal to 3")
    }else{
        return("i am other")
    }

}
let num=_number(9)
console.log(num)

console.log()

// i) make a function that takes input as number and return output You can watch movies if input is greater or equal to 18 else return "You are not authorized to watch movies

let movie=function(age){
    if(age>=18){
        return("You can watch movies")
    }else{
        return("You are not authorized to watch movies")
    }

}
let _movie=movie(20)
console.log(_movie)


