let sum=function(a,b){
    let c=a+b
    return c;     //it returns the value at func call before the end of function defination
}
console.log("hello")

let s=sum(1,2)   //return replaces the function with the value
console.log("end")
console.log(s)

/* 
conclusion:
function without return : call => sum()
function with return: call => let a =sum()
    {keep it in a variable}
 */