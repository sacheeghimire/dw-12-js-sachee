// OR can be used as default value.

let name = "Sachee" || "ram" || "hari"
console.log(name)

// It watches truly value, if not move to other. If all are falsy then print the last value.

let _name = false || "" || undefined || null || 0
console.log(_name) //0