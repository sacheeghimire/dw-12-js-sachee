// nullish ??

let _name = false ?? "" ?? undefined ?? null ?? 0
console.log(_name)

// It watches truly value, if not move to other.
//  If all are falsy then print the last value.
// In the case of nullish, falsy values are (null,undefined)