
let list=["a","abcd","bc"]

//sort=>ascending
//sort according to length.

let newList=list.sort((a,b)=>{
    return a.length-b.length
})
console.log(newList)

//sorting these array of objects according to the price.

let products=[
    {name:"earphone",price:1000},
    {name:"battery",price:2000},
    {name:"charger",price:100},
]
let sortProduct = products.sort((a,b)=>{
    return a.price-b.price
})
console.log(sortProduct)