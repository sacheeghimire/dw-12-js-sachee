
// To compute number sort we should use arrow func.

// ascending sort:

let list = [0,8,1,9,10]

let _list=list.sort((a,b)=>{
    return a-b  //ascending sort
})
console.log(_list)

//descending sort

let listArr = [0,8,1,9,10]

let _listArr=listArr.sort((a,b)=>{
    return b-a  //descending sort
})
console.log(_listArr)
