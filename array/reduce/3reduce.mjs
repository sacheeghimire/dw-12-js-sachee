// find sum of all even numbers using reduce.
// filter and reduce
 
let list=[1,2,3,4]

let _evenList=list.filter((value,i)=>{
    if(value%2===0){
        return true
    }else{
        return false
    }

})//[2,4]

let sumOfEven = _evenList.reduce((pre,cur)=>{
    return pre+cur
},0)
console.log(sumOfEven)//6