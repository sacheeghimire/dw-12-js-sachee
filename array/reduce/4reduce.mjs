//Find the sum of all products.

let products = [
    {name: "earphone", price:1000},
    {name: "battery", price:2000},
    {name: "charger", price:3000},
]

let productsPrice = products.map((value,i)=>{

    //value={name: "earphone", price:1000}

    return value.price
}) // productsPrice=[1000,2000,3000]

let sumOfAllProduct = productsPrice.reduce((pre,cur)=>{
    return pre+cur
},0)
console.log(sumOfAllProduct)