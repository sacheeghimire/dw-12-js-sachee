/*
It has 3 parts:
*syntax,
*execution
*uses 
 */

//Sum of all elements.

let money = [10000000, 30000000, 50000000];

//                         previous, current
//                         0 , 1C
//                         1C , 3C
//                         4C,5C

let total = money.reduce((pre, cur) => {
  return pre + cur;
}, 0); //1C//4C//9C

console.log(total); // 9C

