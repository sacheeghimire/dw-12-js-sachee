// array is used to define or store multiple values
let names=["sachee","lachee","ruchee", 29, false]
//indexing [0,          1,       2]


//array is used to store the data of different types or same type.

//retrieving all elements
console.log(names)

//retrieving specific data
console.log(names[1]) 
console.log(names[0]) 
console.log(names[2]) 
//to call the particular value with the help of indexing

//changing elements of array
names[2]="hari"
console.log(names)

//CTRL+d to change  particular words at same time.