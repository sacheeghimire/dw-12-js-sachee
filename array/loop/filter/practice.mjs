//["a","b","c"] see if there is b or not.

let ar=["a","b","c"]
let ar2=ar.filter((value,i)=>{
    if(value ==="a" || value==="d"){
        return true
    }else{
        return false
    }
})
console.log(ar2) //[a] returns array.

// return positive numbers

let input=[1,-9,10,-10,6,-6]
let output = input.filter((value,i)=>{
    if(value>0){
        return true
    }
    else{
        return false
    }
})
console.log(output)

//filter string ["a",1,"b",2] = ["a","b"]

let inputS=["a",1,"b",2] 
let outputS=inputS.filter((value,i)=>{
     if(typeof(value)==="string"){
          return true
     }else{
          return false
     }
})
console.log(outputS)

//filter truly value only  {for falsy !value}
//where number=0 is falsy ;other truly.
//"" empty string is falsy other are truly.

let all=["a",1,"b",2,0,undefined,false,true]
// let str= all.filter((value,i)=>{
//     if(value){
//         return true
//     }else{
//         return false
//     }
// })
// console.log(str)

//for this program the one line code.

let str=all.filter(Boolean)
console.log(str)



