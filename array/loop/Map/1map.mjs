//Map always needs a return func. 
//It produces an array. It fills the array with value.

let ar1=["s","a","c","h","e","e"]

// It is stored in a variable because it has to return the value.

let ar2=ar1.map((value,index)=>{
    return value
})
console.log(ar2)

//Another example

let ar3=["s","a","c","h","e","e"]

let ar4=ar3.map((value,index)=>{
    return 3
})
console.log(ar4) //[3,3,3,3,3,3]

