// //convert "my" to "My".
// let first="my"
// let _first=first.split("").map((value,i)=>{
//     if(i===0){
//         return value.toUpperCase()
//     }else{
//         return value.toLocaleLowerCase()
//     }

// })//.join
// ["M","y"] split is on array

// let second=_first.join("")
// console.log(second)

//Make a function to make *** firstLetterCapital ***

let firstLetterCapital = (input) => {
  let inputArr = input.split("");
  let inputArrArr = inputArr.map((value, i) => {
    if (i === 0) {
      return value.toUpperCase();
    } else {
      return value.toLocaleLowerCase();
    }
  });

  let output = inputArrArr.join("");
  return output;
};
let _firstLetterCapital = firstLetterCapital("my");
console.log(_firstLetterCapital);



