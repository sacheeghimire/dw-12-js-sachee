//[1,2,3,4]=>[2,0,6,0] here odd index is multiplied by zero and even is multiplied by 2.

let ar1=[1,2,3,4]
let ar2=ar1.map((value,i)=>{
    if(i%2===0){
        return value * 2
    }else{
        return value *0
    }
})
console.log(ar2)

//["s","a","C","h","e","e"] => ["S","a","c","h","e","e"]

let name = ["s","a","C","h","e","e"]
let _name = name.map((value,i)=>{
    if(i===0){
        return value.toUpperCase()
    }
    else{
        return value.toLocaleLowerCase()
    }
})
console.log(_name)


// [1,2,3,4]=>[2,4,6,8] using map and function

let fun=(inputArr)=>{
    let outputArr = inputArr.map((value,i)=>{
        return value *2
    })
    return outputArr
}
let _fun=fun([1,2,3,4])
console.log(_fun)

//Importing function from multipleof2.mjs

let _fun1 = fun([1,6,3,4]) //CTRL + space to import
console.log(_fun1)

