
let ar1=[1,2,3.4]

/*

Use map if:

* input and output are array
*input and output length are same. 

*/

//  ar2=[]
//  ar2=[2]
//  ar2=[2,4] ....

let ar2=ar1.map((value,i)=>{
    return value*2
})
console.log(ar2) // [2,4,6,8]