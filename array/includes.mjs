//Ture or False if it contains inside the given array or not.

let ar1=["sachee", 29, false];
let ar2= ar1.includes(29)
console.log(ar2)

//lets suppose we have roles lists as ["admin","superAdmin","customer"].Check whether the array has admin.

let proff= ["admin","superAdmin","customer"]
let Has=proff.includes("admin")
console.log(Has)

//Using includes method find weather we have bearer in the string "Bearer token."

let str = "Bearer token"
let _str = str.split(" ").includes("Bearer")
console.log(_str)