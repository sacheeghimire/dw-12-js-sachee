// Imp of memory queue.

setTimeout(() => {
  console.log("2 sec");
}, 2000);

setTimeout(() => {
  console.log("0 sec");
}, 0);

setTimeout(() => {
  console.log("1 sec");
}, 1000);

/*

any task that takes 5 sec.
=>a.

 */

/*

Memory queue is needed to store the asynchronous function according to their time slot.
It works in FIFO principle.

*/
