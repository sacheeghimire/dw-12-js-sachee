
// *Any thing that push it's task to the background(node) are called asynchronous function.

// *During code execution, the background code will execute only when all synchronous code(js code) gets executed.

// *Call stack run the code inside it onces the code gets executed, the code is popped-off.  

/*

 EVENT LOOP : It is a mediator which continuously monitors call stack and memory queue. If the call stack is empty it push the function from
              memory queue to call stack.
 
*/

 