// find the array of id i.e.

let products = [
  {
    id: 1,
    title: "Product 1",
    category: "electronics",
    price: 5000,
    description: "This is description and Product 1",
    discount: {
      type: "a1",
    },
  },
  {
    id: 2,
    title: "Product 2",
    category: "cloths",
    price: 2000,
    description: "This is description and Product 2",
    discount: {
      type: "a2",
    },
  },
  {
    id: 3,
    title: "Product 3",
    category: "electronics",
    price: 3000,
    description: "This is description and Product 3",
    discount: {
      type: "a3",
    },
  },
];

let ids = products.map((value, i) => {
  return value.id;
});
console.log(ids); //[1,2,3]

// `${}` for string

//find the array of title ie output must be ["Product 1", "Product 2", "Product 3"
let _product = products.map((value, i) => {
  return value.title;
});
console.log(_product);

//array of category

let _arr = products.map((value, id) => {
  return value.category;
});
console.log(_arr);

//array of type.

let _type = products.map((v, i) => {
  return v.discount.type;
});
console.log(_type); //[ 'a1', 'a2', 'a3' ]

//find the array of price where each price is multiplied by 3  output must be [ 15000,6000,9000]

let _price = products.map((value, i) => {
  return `${value.price * 3}`;
});
console.log(_price);

let _ar = products.filter((value, i) => {
  if (value.price >= 3000) return true;
});
console.log(_ar);
// output: {
//     id: 1,
//     title: 'Product 1',
//     category: 'electronics',
//     price: 5000,
//     description: 'This is description and Product 1',
//     discount: { type: 'a1' }
//   },
//   {
//     id: 3,
//     title: 'Product 3',
//     category: 'electronics',
//     price: 3000,
//     description: 'This is description and Product 3',
//     discount: { type: 'a3' }
//   }
// ]

//find those array of  title whose price is >= 3000=>["product 1",product 3

let _array=products.filter((value,i)=>{
    if (value.price >= 3000) return true;
});
let _arr1=_array.map((value,i)=>{
    return value.title
})
console.log(_arr1)//[ 'Product 1', 'Product 3' ]