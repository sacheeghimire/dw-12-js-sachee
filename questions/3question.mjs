let products = [
  {
    id: 1,
    title: "Product 1",
    category: "electronics",
    price: 5000,
    description: "This is description and Product 1",
    discount: {
      type: "a1",
    },
  },
  {
    id: 2,
    title: "Product 2",
    category: "cloths",
    price: 2000,
    description: "This is description and Product 2",
    discount: {
      type: "a2",
    },
  },
  {
    id: 3,
    title: "Product 3",
    category: "electronics",
    price: 3000,
    description: "This is description and Product 3",
    discount: {
      type: "a3",
    },
  },
];

//["product1","products3"]
//If filter and map are used simultaneously, always use filter first.

let _product = products
  .filter((value, i) => {
    if (value.price >= 3000) return true;
  })
  .map((value, i) => {
    return value.title;
  });
console.log(_product);

//find those array of title whose price does not equal to 5000 ==> ["product 2","product 3"]

let _product5000 = products
  .filter((value, i) => {
    if (value.price !== 5000) return true;
  })
  .map((value, i) => {
    return value.title;
  });
console.log(_product5000); //[product2,product3]

//find those array of category whose price equal to 3000 ====> ["electronics"]
let _equal5000 = products
  .filter((value, i) => {
    if (value.price == 3000) return true;
  })
  .map((value, i) => {
    return value.category;
  });
console.log(_equal5000);

//map is used to modify elements
//filter is used to filter elements of input.
