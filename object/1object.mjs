// Object is represented in {}.
// It is used to store multiple data along with information.
// It has 3 important parts: property= key + value.

let info = {
  name: "Sachee",
  age: 22,
  weight: 49,
  isMarried: false,
};
console.log(info.name);
console.log(info.weight);
console.log(info.isMarried);
console.log(info.age);

//to change the age

info.age = 23;

console.log(info);

//to delete weight.

delete info.weight;
console.log(info);
