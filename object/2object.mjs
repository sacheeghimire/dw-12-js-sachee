let info = {
  name: "Sachee",
  age: 22,
  isMarried: false,
};
//["name","age","isMarried"]// keys
//["Sachee",22,false]  //values
//[["name","Sachee"],["age",22],["isMarried",false]]// properties, entries

let keysArray = Object.keys(info);
console.log(keysArray);

let valuesArray = Object.values(info);
console.log(valuesArray);

let propertiesArray = Object.entries(info);
console.log(propertiesArray);

// *Object to Array.
