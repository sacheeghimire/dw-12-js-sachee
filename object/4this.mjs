

let info={
    name:"Sachee",
    surname:"Ghimire",
    age:22,
    fullName : function(){
        console.log(`My name is ${this.name} ${this.surname}`) // 'this' is used to call itself/points itself.
    }
}
console.log(info.name)
info.fullName()

// * Arrow Function does not support 'this'.