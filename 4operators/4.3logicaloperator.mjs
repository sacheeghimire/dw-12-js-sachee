// && , ||(or), !(not)
//&& true if all are true.
console.log(true&&true)
console.log(true&&false)
console.log(true&&true&&false)
// || true if one is true
console.log(true||true)
console.log(true||false)
console.log(false||false||false)
// coverts to opposite value !
console.log(!true)//fasle
console.log(!false)//true