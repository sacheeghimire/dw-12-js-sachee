let name= "Sachee"
let surname="Ghimire"
let fullName= `my name is ${name} and surname is ${surname}` //`` should be with ${}
console.log(fullName)

/* 
string can be defined using "" , ' ' , ` `
we can call variables inside `` using ${} but it is not possible in '' , ""
we can not use ' inside ' ' and so on.
 */